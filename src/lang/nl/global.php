<?php

return [
    'requiredFieldsExplanation' => 'Velden met een<span style="color: #ff0000"> * </span>zijn verplicht',

    'Cancel' => 'Annuleren',
    'Save' => 'Opslaan',

];