<?php namespace Jcgroep\ConfigureIt;

use Illuminate\Support\ServiceProvider;

class ConfigureItServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/lang/', 'ConfigureIt');
        $this->loadViewsFrom(__DIR__ . '/views/', 'ConfigureIt');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}