<?php namespace Jcgroep\ConfigureIt;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Use this class to search in the comments table
 * Class SettingsSearchService
 * @package App\SearchServices
 */
class SettingsSearchService
{

    protected $group;
    protected $key;
    protected $onlyViewable = false;
    protected $isOverridable;
    protected $subGroup;

    public static function make()
    {
        return new static;
    }

    /**
     * @param string $group
     * @return SettingsSearchService
     */
    public function withGroup($group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @param string $key
     * @return SettingsSearchService
     */
    public function withKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return SettingsSearchService
     */
    public function onlyViewable()
    {
        $this->onlyViewable = true;
        return $this;
    }

    /**
     * @param string $groupKey
     * @return SettingsSearchService
     */
    public function withGroupKey($groupKey)
    {
        list($this->group, $this->subGroup, $this->key) = explode('.', $groupKey,3);
        return $this;
    }

    /**
     */
    public function withOverridable($boolean)
    {
        $this->isOverridable = $boolean;
        return $this;
    }


    public function query()
    {
        $query = \Setting::where(
            function (Builder $query) {
                if (isset($this->key)) {
                    $query->where('key', '=', $this->key);
                }
                if (isset($this->group)) {
                    $query->where('group', '=', $this->group);
                }
                if (isset($this->subGroup)) {
                    $query->where('sub_group', '=', $this->subGroup);
                }
                if ($this->onlyViewable) {
                    $query->where('show_in_interface', '=', true);
                }
                if ($this->isOverridable) {
                    $query->where('is_overridable', $this->isOverridable);
                }
            }
        );

        return $query;
    }

    public function groupedByGroupAndSortedByTrans()
    {
        $result = $this->query()
            ->get()
            ->groupBy(
                function ($item) {
                    return trans('settings.' . $item->group . '.groupName');
                }
            )->all();

        ksort($result);

        return Collection::make($result);
    }

    public function groupedByGroupBySubGroup()
    {
        $result = $this->query()
            ->get()
            ->groupBy('group')
            ->all();

        $groupByGroupAndSubGroup = [];

        foreach ($result as $groupName => $settings) {
            $groupBySubGroup = [];

            foreach ($settings as $setting) {
                $groupBySubGroup[$setting->sub_group][] = $setting;
            }
            ksort($groupBySubGroup);

            $groupByGroupAndSubGroup[$groupName] = $groupBySubGroup;
        }

        ksort($groupByGroupAndSubGroup);
        return Collection::make($groupByGroupAndSubGroup);
    }
}
