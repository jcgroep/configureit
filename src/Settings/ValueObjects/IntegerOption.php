<?php namespace Jcgroep\ConfigureIt\Settings\ValueObjects;


class IntegerOption extends SettingOption
{

    public function isValid($value = null)
    {
        if ($value !== null) {
            return is_numeric($value);

        }
        return is_numeric($this->value);
    }

    public function getInputElement()
    {
        return '<input id="input-' . str_replace('.', '-', $this->key) . '" type="number" min="0" value="' . $this->value . '" class="' . $this->getClasses() . '"/>';
    }
}
