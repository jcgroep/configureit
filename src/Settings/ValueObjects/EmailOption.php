<?php namespace Jcgroep\ConfigureIt\Settings\ValueObjects;


class EmailOption extends SettingOption
{
    public function isValid($value = null)
    {
        $regex = '/.+@.+\..+/';
        if ($value == null) {
            return preg_match($regex, $this->value);
        }
        return preg_match($regex, $value);
    }

    public function getInputElement()
    {
        return '<input id="input-' . str_replace('.', '-', $this->key) . '" "type="email" value="' . $this->value . '" class="' . $this->getClasses() . '"/>';
    }
}
