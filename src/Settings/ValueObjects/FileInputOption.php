<?php namespace Jcgroep\ConfigureIt\Settings\ValueObjects;


class FileInputOption extends SettingOption
{

    public function showExample()
    {
        return true;
    }

    public function isValid($value = null)
    {
        // check of path bestaat voor gekozen file en is type image
        return true;
    }

    /**
     * @return string
     */
    public function getInputElement()
    {
        return Form::file(
            'file', [
            'id' => str_replace('.', '-', $this->key),
            'value' => $this->value,
            'class' => $this->getClasses(),
            'data-file-too-big-text' => trans('exercises.FileTooBigText')
            ]
        );
    }
}
