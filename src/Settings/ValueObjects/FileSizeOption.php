<?php namespace Jcgroep\ConfigureIt\Settings\ValueObjects;


class FileSizeOption extends IntegerOption
{
    public function getValue()
    {
        return $this->value;
    }

    public function __toString()
    {
        return (string) ($this->getValue()  / 1024 / 1024);
    }

    public function translateValue()
    {
        return $this->__toString();
    }
}
