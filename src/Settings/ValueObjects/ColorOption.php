<?php namespace Jcgroep\ConfigureIt\Settings\ValueObjects;


class ColorOption extends SettingOption
{

    public function isValid($value = null)
    {
        if (strlen($value) == 7) {
            return true;
        }

        if (strlen($this->value) == 7) {
            return true;
        }
        return false;
    }

    public function getInputElement()
    {
        return '<input id="input-' . str_replace('.', '-', $this->key) . '" type="color" value="' . $this->value . '" class="' . $this->getClasses() . '"/>';
    }

    public function translateValue()
    {
        return "<span style='background: " . $this->value . "'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> " . $this->value;
    }
}
