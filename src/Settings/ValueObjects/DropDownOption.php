<?php namespace Jcgroep\ConfigureIt\Settings\ValueObjects;


use Jcgroep\BuildIt\FormElements\SimpleDropdownElement;

abstract class DropDownOption extends SettingOption
{
    public function isValid($value = null)
    {
        return array_key_exists($value, $this->getOptions());
    }

    protected abstract function getOptions();

    public function getInputElement()
    {
        return SimpleDropdownElement::create()
            ->withOptions($this->getOptions())
            ->withName($this->key)
            ->withClasses(['col-md-4', 'col-xs-4', 'col-sm-4', $this->getClasses()])
            ->withId('input-' . str_replace('.', '-', $this->key))
            ->withDefaultValue($this->value)
            ->withJsonData($this->getOptions())
            ->renderElement();
    }

    public function translateValue()
    {
        if(array_key_exists($this->value, $this->getOptions())){
            return $this->getOptions()[$this->value];
        }
        return $this->value;
    }

}
