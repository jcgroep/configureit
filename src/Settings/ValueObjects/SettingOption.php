<?php namespace Jcgroep\ConfigureIt\Settings\ValueObjects;


use InvalidArgumentException;
use Setting;

abstract class SettingOption
{
    protected $key;
    protected $value;
    protected $classes = 'value-object-field';

    public function showExample()
    {
        return false;
    }

    public function openModal()
    {
        return false;
    }

    public function __construct($key, $value)
    {
        if (!$this->isValid($value)) {
            throw new InvalidArgumentException('Value ' . $value . ' is not valid for field ' . $key . ' in class ' . get_class());
        }
        $this->key = $key;
        $this->value = $value;
    }

    public abstract function isValid($value = null);

    public abstract function getInputElement();

    /**
     * @return String
     */
    public function getKey()
    {
        return $this->key;
    }

    public function getHtmlKey()
    {
        return str_replace('.', '-', $this->getKey());
    }

    public function dependsOn()
    {
        return [];
    }

    public function onlyOverridableAsRoles()
    {
        return [];
    }

    public function translateValue()
    {
        return ucfirst($this->value);
    }

    public function hasRoleToOverride(\User $user = null)
    {
        if (! isset($user)) {
            $user = Auth::user();
        }
        $roles = $this->onlyOverridableAsRoles();
        if (count($roles) == 0) {
            return true;
        }
        foreach ($roles as $role) {
            if ($user->hasRole($role)) {
                return true;
            }
        }
        return false;
    }

    public function isVisible(\User $user = null)
    {
        if (! $this->hasRoleToOverride($user)) {
            return false;
        }
        foreach ($this->dependsOn() as $dependant => $value) {
            if (Setting::get($dependant, $user) != $value) {
                return false;
            }
        }
        return true;
    }

    public function getDependsOnJavascript()
    {
        $script = '';
        foreach ($this->dependsOn() as $dependant => $value) {
            $script .= "$(document).on('change', '#input-" . str_replace('.', '-', $dependant) . "', function(){
				if($(this).val() != $value){
				    $('#setting-".$this->getHtmlKey()."').hide();
				}else{
					$('#setting-".$this->getHtmlKey()."').show();
				}
			});\n";
            $script .= "$('#input-" . str_replace('.', '-', $dependant) . "').trigger('change');";
        }
        return $script;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value;
    }

    /**
     * @return String
     */
    public function getClasses()
    {
        return $this->classes;
    }
}
