<?php namespace Jcgroep\ConfigureIt\Settings\ValueObjects;

class BooleanDropdown extends DropDownOption
{

    public function isValid($value = null)
    {
        if (is_bool($value) || in_array($value, ['true', 'false', '1', 1, '0']) || $value === 0) {
            return true;
        }
        return false;
    }

    protected function getOptions()
    {
        return [true => trans('global.Yes'), false => trans('global.No')];
    }

    public function translateValue()
    {
        return $this->getOptions()[$this->value];
    }

    public function getValue()
    {
        return (int) $this->value;
    }
}
