<?php namespace Jcgroep\ConfigureIt\Settings\ValueObjects;

use Lang;

class TextareaOption extends TextOption
{
    public function getInputElement()
    {
        return '<textarea id="input-' . str_replace('.', '-', $this->key) . '" class="col-md-11 ' . $this->getClasses() . '">' . $this->value . '</textarea>';
    }
}
