<?php namespace Jcgroep\ConfigureIt\Settings\ValueObjects;


use Lang;

class StringOption extends SettingOption
{
    public function isValid($value = null)
    {
        return true;
    }

    public function getInputElement()
    {
        return '<input id="input-' . str_replace('.', '-', $this->key) . '" value="' . $this->value . '" class="' . $this->getClasses() . '"/>';
    }

    public function getValue()
    {
        if(Lang::has('settings.' . $this->value)) {
            return trans('settings.' . $this->value);
        }
        return parent::getValue();
    }
}
