<?php namespace Jcgroep\ConfigureIt\Settings\ValueObjects;

use Lang;

class TextOption extends SettingOption
{
    public function isValid($value = null)
    {
        return true;
    }

    public function getInputElement()
    {
        return '<textarea id="input-' . str_replace('.', '-', $this->key) . '" class="col-md-11 summernote ' . $this->getClasses() . '">' . $this->value . '</textarea>';
    }

    public function getValue()
    {
        if(Lang::has('settings.' . $this->value)) {
            return trans('settings.' . $this->value);
        }
        return parent::getValue();
    }
}
