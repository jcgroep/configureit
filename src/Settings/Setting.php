<?php namespace Jcgroep\ConfigureIt\Settings;

use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;
use Jcgroep\ConfigureIt\SettingsSearchService;
use StringObject;

class Setting extends \BaseModel
{
    protected $fillable = [
        'group',
        'sub_group',
        'key',
        'value',
        'show_in_interface',
        'editable',
        'value_object',
        'is_overridable',
    ];

    /**
     * @param $key
     * @param $value
     * @throws InvalidArgumentException
     */
    public static function set($key, $value)
    {
        $setting = SettingsSearchService::make()
            ->withGroupKey($key)
            ->query()
            ->first();

        $setting->getSettingOption($value);

        $setting->update([
            'value' => $value,
        ]);
        return $setting;
    }

    protected function getValueObjectAttribute($className)
    {
        if (class_exists($className)) {
            return $className;
        }
        return 'Jcgroep\\ConfigureIt\\Settings\\ValueObjects\\' . $className;
    }

    public function getSettingOption($newValue = null)
    {
        return (new $this->value_object($this->toString(), $newValue == null ? $this->value : $newValue));
    }

    public static function has($key)
    {
        return SettingsSearchService::make()
            ->withGroupKey($key)
            ->query()
            ->count() > 0;
    }


    public static function findByKey($key)
    {
        $setting = SettingsSearchService::make()
            ->withGroupKey($key)
            ->query()
            ->first();

        if (!isset($setting)) {
            throw new InvalidArgumentException('Group.Key combination ' . $key . ' not found in the settings table');
        }
        return $setting;
    }

    /**
     * @param String $key need to be a String of group and key values separated by a dot
     * @return mixed
     */
    public static function get($key)
    {
        return self::find($key)->value;
    }

    public function toString()
    {
        return $this->group . '.' . $this->sub_group . '.' . $this->key;
    }
}
