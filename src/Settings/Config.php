<?php namespace Jcgroep\ConfigureIt\Settings;


use Illuminate\Config\Repository;

class Config extends Repository
{
    public function has($key)
    {
        if(Setting::has($key)) {
            return true;
        }
        return parent::has($key);
    }

    public function get($key, $default = null)
    {
        if(Setting::has($key)) {
            return Setting::get($key)->value;
        }
        return parent::get($key, $default);
    }
}
