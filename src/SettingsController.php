<?php namespace Jcgroep\ConfigureIt;

use Flash;
use Illuminate\Routing\Controller;
use Input;
use Jcgroep\ConfigureIt\Settings\Setting;
use Lang;
use Redirect;

class SettingsController extends Controller
{
    public function index()
    {
        $settingGroups = SettingsSearchService::make()
            ->onlyViewable()
            ->groupedByGroupBySubGroup();

        return view('ConfigureIt::index', compact('settingGroups', 'smsPerMonth', 'agreements'));
    }

    public function update()
    {
        try {
            $setting = Setting::find(Input::get('settingId'));
            Setting::set($setting->toString(), Input::get('value'));
            return [
                'message' => trans('settings.SettingUpdated')
            ];
        } catch (\InvalidArgumentException $e) {
            return [
                'errors' => trans('validation.justWrong')
            ];
        }
    }

    public function uploadImage()
    {
        $image = Input::file('image');

        $relativePath = '/images/settingsPictures/uploaded/';
        $name = Input::get('settingKey');
        $settingKey = 'layout.images.' . $name;

        $fileName = FileManager::uploadImage($image, '/app' . $relativePath, $name, basename(Setting::get($settingKey)), []);

        Setting::set($settingKey, $relativePath . $fileName);

        Flash::success(trans('settings.ImageUploadedSuccess'));

        return Redirect::back();
    }
}