@foreach($group as $subgroupName => $settings)
    <h2>{{ trans('settings.' . $groupName . '.subGroups.' . $subgroupName) }}</h2>
    @if(Lang::has('settings.' . $groupName . '.introTexts.' . $subgroupName))
		<p>
			{{trans('settings.' . $groupName . '.introTexts.' . $subgroupName)}}
		</p>
	@endif
    <table id="settings-index" class="table table-striped table-bordered table-hover">
        <tbody id="table-body-team-patients">
        @foreach($settings as $setting)
            @include('ConfigureIt::setting')
        @endforeach
        </tbody>
    </table>
@endforeach