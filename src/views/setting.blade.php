<tr id="setting-{{str_replace('.', '-', $setting->toString())}}">
	<td class="col-md-6">
		{{ ucfirst(trans('settings.' . $setting->group . '.'. $setting->key)) }} {{ $setting->is_overridable ? trans('settings.PatientCanOverride') : '' }}
	</td>
	@if($setting->getSettingOption()->showExample())
		<td class="col-md-6">
			<form method="POST" action="{{route('settings.uploadImage')}}#group-layout" class="form-horizontal" enctype="multipart/form-data">
				{{--{!! Form::open(['route' => 'settings.uploadImage', 'class' => 'form-horizontal', 'files' => true]) !!}--}}
				{{ csrf_field() }}
				<label for="image"></label>
				{!! Form::hidden('settingKey', $setting->key) !!}
				{!! Form::file('image', ['accept' => 'image/*', 'onchange' => 'this.form.submit()', 'class' => 'small-text', 'data-file-too-big-text' => trans('exercises.FileTooBigText')]) !!}
			</form>
		</td>
	@else
		<td class="col-md-6" style="border-right:0">
			<form>
	            <span class="show_value">

				@if($setting->editable)
					<input type="submit" onclick="return false;"
						id="edit-{{str_replace('.', '-', $setting->toString())}}"
						class="fa pull-right" value="&#xf040;"/>
				@endif

				<span class="value ">{{ $setting->getSettingOption()->getValue() }}</span>

				@if (Lang::has('settings.' . $setting->group . '.'. $setting->key . "Postfix"))
	                    {{ trans('settings.' . $setting->group . '.'. $setting->key . "Postfix") }}
				@endif

	            </span>
	            <span class="edit_value" style="display: none">
	                @if(isset($setting->value_object))
	                    {!! $setting->getSettingOption()->getInputElement() !!}
	                @else
	                    <input id="input-{{str_replace('.', '-', $setting->toString())}}"
	                           value="{{ $setting->value }}"/>
	                @endif

	                @if (Lang::has('settings.' . $setting->group . '.'. $setting->key . "Postfix"))
	                    {{ trans('settings.' . $setting->group . '.'. $setting->key . "Postfix") }}
	                @endif

	                <input type="submit" onclick="return false;"
	                       id="save-{{str_replace('.', '-', $setting->toString())}}"
	                       data-setting-id="{{ $setting->id }}"
	                       class="fa pull-right" value="&#xf0c7;"/>
	            </span>
			</form>
		</td>
	@endif
	@if($setting->getSettingOption()->showExample())
		<td>
			<img src="{{Setting::has('layout.images.' . $setting->key) ? Setting::get('layout.images.' . $setting->key) : '/images/settingsPictures/defaultImage.png'}}"
			     height="50px" alt="{{$setting->key}}">
		</td>
	@else
		@include('alerts.ajaxLoadingIconTable')
	@endif
</tr>