<ul class="nav nav-tabs" id="manage-settings">
	@foreach($groups as $groupName => $group)
		<li>
			<a href="#group-{{$groupName}}" data-toggle="tab">
				{{ ucfirst(trans('settings.' . $groupName . '.groupName')) }}
			</a>
		</li>
	@endforeach
</ul>

<div class="tab-content" id="tab-content">
	@foreach($settingGroups as $groupName => $group)
		<div class="tab-pane fade in" id="group-{{$groupName}}">
			@include('ConfigureIt::settingGroup')
		</div>
	@endforeach
	@yield('additionalTabs')
</div>

<script>
	@foreach($settingGroups as $translatedGroup => $settingSubGroups)
		@foreach($settingSubGroups as $subGroup => $settings)
			@foreach($settings as $setting)
				{!! $setting->getSettingOption()->getDependsOnJavascript() !!}
			@endforeach
		@endforeach
	@endforeach

	$(document).on('click', '.show_value input[type="submit"]', function () {
	        $(this).parents('.show_value').hide().closest('td').find('.edit_value').show();
	    });

	    var validateSetting = function (field) {
	        if (field.attr('type') == 'email') {
	            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	            return re.test(field.val());
	        }
	        return true;
	    };

	    $(document).on('click', '.edit_value input[type="submit"]', function () {
	        var saveButton = $(this);
	        var parentEditSpan = saveButton.parents('.edit_value');
	        var valueObjectField = parentEditSpan.find('.value-object-field');
	        var value = valueObjectField.val();
	        var jsonData = valueObjectField.data('json');
	        var showValue = value;

	        if (validateSetting(parentEditSpan.find('input')) === true) {
	            if (jsonData) {
	                showValue = jsonData[value];
	            }
	            parentEditSpan.hide().closest('td').find('.show_value').show().find('.value').html(showValue);
	            ajaxIcon.load(parentEditSpan);
	            $.ajax({
	                type: 'PUT',
	                url: 'settings/update',
	                data: {
	                    'settingId': saveButton.data('setting-id'),
	                    'value': value
	                },
	                success: function(){
	                    ajaxIcon.finish(parentEditSpan);
	                }
	            });
	            parentEditSpan.find('input').css('background-color', 'white');
	        } else {
	            parentEditSpan.find('input').css('background-color', 'red');
	        }
	    });
</script>